clc;clear all;
%Parte responsavel por chamar as funções e capturar as entradas do usuario.
prompt = {'Digite a frequencia do sinal em Hz:','Digite o multiplicador da frequencia de Nyquist (valores maiores que 2):','Digite a amplitude do sinal:','Digite a duração do sinal em s','Quantidades de bits do conversor'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {'5','3','10','5','8'};
options.Resize='on';
resp = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
for i=1:length(resp)
    check = str2num(resp{i});
    if isempty(check) || i==2 && check<2
        uiwait(msgbox('Valor invalido', 'Error','error'));
        prompt = {'Digite a frequencia do sinal em Hz:','Digite o multiplicador da frequencia de Nyquist (valores maiores que 2):','Digite a amplitude do sinal:','Digite a duração do sinal em s','Quantidades de bits do conversor'};
        dlg_title = 'Input';
        num_lines = 1;
        defaultans = {'5','3','10','5','8'};
        options.Resize='on';
        resp = inputdlg(prompt,dlg_title,num_lines,defaultans,options);
        i=0;
    end
end
% Chamadas das funções.
[s,s2,t,t2]=signal(str2num(resp{1}),str2num(resp{2}),str2num(resp{3}),str2num(resp{4}));
res=QuantDB(s2,str2num(resp{5}));
% Criação dos graficos.
subplot(2,1,1)
plot(t,s);
hold on
stem(t2,s2,'g');
stairs(t2,res,'r');
hold off
title('Seno');
xlabel('Tempo(s)');
ylabel('Amplitude');
subplot(2,1,2)
stem(t2,res,'filled');
title('Seno Quantizado');
xlabel('Tempo(s)');
ylabel('Amplitude');